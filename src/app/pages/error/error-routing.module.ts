import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ErrorComponent } from './error/error.component';

const routes: Routes = [   
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'not-found'
    },
    {
        path: 'not-found',
        component: ErrorComponent
    }
]

@NgModule({
    declarations: [],
    imports: [ CommonModule, RouterModule.forChild(routes) ],
    exports: [RouterModule],
    providers: [],
})
export class ErrorRoutingModule {}