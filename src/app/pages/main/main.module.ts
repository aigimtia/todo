import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MainRoutingModule } from './main-routing.module';
import { MatCardModule } from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormComponent } from './form/form.component';

@NgModule({
    declarations: [
       DashboardComponent,
       FormComponent
    ],
    imports: [ 
        CommonModule, 
        FormsModule,
        MainRoutingModule, 
        ReactiveFormsModule,
        MatCardModule,
        MatIconModule,
        MatDialogModule
    ],
    exports: [],
    providers: [],
})
export class MainModule {}