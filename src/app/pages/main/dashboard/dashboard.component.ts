import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/core/interfaces/todo.interface';
import { data } from 'src/app/core/static/data';
import { MatDialog } from '@angular/material/dialog';
import { FormComponent } from '../form/form.component';
import { DataShareService } from 'src/app/core/services/data.share.service';
import { SwalService } from 'src/app/core/services/swal.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public todoList: Todo[]=[];
  public status = false;

  constructor(public dialog: MatDialog, private dataShareService: DataShareService,
              public swalService: SwalService) { }

  ngOnInit() {
    if(localStorage.getItem('data')) {
      this.todoList = JSON.parse(localStorage.getItem('data'))
    } else {
      this.todoList = data;
    }

    this.dataShareService.sharedUpdatedTodo.subscribe(list => {
      if(list) {
        this.todoList = list;
      }
    })
  }

  openModal(): void {
    this.dataShareService.setTodoItem(null);
    const dialogRef = this.dialog.open(FormComponent,{
      width: '640px',disableClose: true 
    });
  } 

  public updateStatus(todo) {
    this.todoList.map(item => {
      if(item.id === todo.id) {
        item.done = !item.done;
        return item;
      }
    });
    localStorage.setItem('data', JSON.stringify(this.todoList))
  }

  public update(todo) {
    this.dataShareService.setTodoItem(todo);
    const dialogRef = this.dialog.open(FormComponent,{
      width: '640px',disableClose: true 
    });
  }

  public delete(id: number) {
      this.swalService.showConfirmDialog().then(async (result) => {
          if (result.value) {
              try {
                for(var i=0; i < this.todoList.length; i++) {
                  if(this.todoList[i].id == id)
                  {
                    this.todoList.splice(i,1);
                  }
                }
                this.swalService.showBasicAlert({
                    message: 'Selected todo item has been deleted!'
                });
                localStorage.setItem('data', JSON.stringify(this.todoList))
              } catch (e) {
                  this.swalService.showErrorAlert();
              }
          }
      });
  }

  public filter(event) {
    if(localStorage.getItem('data')) {
      this.todoList = JSON.parse(localStorage.getItem('data')).filter(todo => todo.content.includes(event.target.value) === true)
    } else {
      this.todoList = data.filter(todo => todo.content.includes(event.target.value) === true);
    }
  }

}
