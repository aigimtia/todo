import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DataShareService } from 'src/app/core/services/data.share.service';
import { data } from 'src/app/core/static/data';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  
  public form: FormGroup;
  public updateBtn: boolean;

  constructor(public dialog: MatDialog, private dataShareService: DataShareService) { }

  ngOnInit() {
    this.form = new FormGroup({
      id: new FormControl(null),
      title: new FormControl(''),
      content: new FormControl(''),
      done: new FormControl(false)
    });

    this.dataShareService.sharedTodoItem.subscribe(todo=> {
      if(todo) {
        this.updateBtn = true;
        this.form.patchValue(todo)
      } else {
        this.updateBtn = false;
      }
    })
  }

  public submit(updateBtn) {
    let temp = JSON.parse(localStorage.getItem('data'))
    if(!updateBtn) {
      if(temp) {
        const maxId = Math.max(...temp.map(o => o.id), 0);
        this.form.value.id = maxId+1;
        temp.unshift(this.form.value)
        localStorage.setItem('data', JSON.stringify(temp))
        this.dataShareService.setUpdatedTodo(temp)
      } else {
        const maxId = Math.max(...data.map(o => o.id), 0);
        this.form.value.id = maxId+1;
        data.unshift(this.form.value)
        localStorage.setItem('data', JSON.stringify(data))
        this.dataShareService.setUpdatedTodo(data)
      }
    } else {
      if(temp) {
        temp.map(item => {
          if(item.id === this.form.value.id) {
            item.title = this.form.value.title;
            item.content = this.form.value.content;
            return item;
          }
        });
        this.dataShareService.setUpdatedTodo(temp)
        localStorage.setItem('data', JSON.stringify(temp))
      } else {
        data.map(item => {
          if(item.id === this.form.value.id) {
            item.title = this.form.value.title;
            item.content = this.form.value.content;
            return item;
          }
        });
        this.dataShareService.setUpdatedTodo(data)
        localStorage.setItem('data', JSON.stringify(data))
      }
    }
    this.dialog.closeAll();
  }

  public cancel() {
    this.dialog.closeAll()    
  }

}
