import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { ErrorModule } from './error/error.module';

@NgModule({
    declarations: [],
    imports: [ 
        CommonModule, 
        PagesRoutingModule,
        ErrorModule
     ],
    exports: [],
    providers: [],
})
export class PagesModule {}