import { Injectable } from '@angular/core';
import Swal, { SweetAlertType } from 'sweetalert2';

@Injectable({
    providedIn: 'root'
})

export class SwalService {
    public constructor() {

    }

    public showConfirmDialog(
        params: { title?: string, text?: string, type?: SweetAlertType, showCancelButton?: boolean, confirmButtonText?: string } = {}
    ) {
        return Swal.fire({
            title: params.title ? params.title : 'Are you sure?',
            text: params.text ? params.text : 'You won\'t be able to revert this!',
            type: params.type ? params.type : 'warning',
            showCancelButton: params.showCancelButton ? params.showCancelButton : true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancel',
            confirmButtonText: params.confirmButtonText ? params.confirmButtonText : 'Yes, delete it!'
        });
    }

    public showBasicAlert(params: any = {}) {
        return Swal.fire(
            params.title ? params.title : 'Deleted!',
            params.message ? params.message : 'Todo item has been deleted.',
            params.type ? params.type : 'success');
    }

    public showErrorAlert(params: any = {}) {
        return Swal.fire(
            'Error occurred!',
            params.message ? params.message : 'Error occurred while processing the request!',
            'error');
    }
}
