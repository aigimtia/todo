import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DataShareService {

    private todoItem = new BehaviorSubject(null);
    sharedTodoItem = this.todoItem.asObservable();
    

    setTodoItem(todo: any) {
        this.todoItem.next(todo);
    }

    private updatedTodo = new BehaviorSubject(null);
    sharedUpdatedTodo = this.updatedTodo.asObservable();

    setUpdatedTodo(list: any) {
        this.updatedTodo.next(list);
    }
}